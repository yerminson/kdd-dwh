La idea era llenar las tres estrellas pero debido a los datos que se tiene es imposible
diferenciar una matricula de una adicion, se decide cargar solamente las estrellas de
matricula y cancelacion, las reactivaciones se toman como una matricula. Se asume que todos los datos
datos corresponden al segundo semestre de 2011.

NOTA: es lo que va hasta el momento, ocurre un error debido a que alguna de las consultas no arroja resultados
y luego le hago getString, toca revisar. Tarda muchisimo, en 5 minutos solo grabó 15000 registros de matricula, quizas
sea por guardar en archivo o no se, por el volumen de datos.
Aun falta todo el agregado


Actualizacion 1 Enero del 2012 (:o Nuevo año :D)
Bueno pues aunque lento pero muy seguro se realizo el proceso de carga de las tres estrellas que habiamos considerado inicialmente
las cuales son las estrella de matricula, la estrella de cancelacion y la estrella de adiccion.

El trabajo para esta carga inicialmente se estaba haciendo generando los scripts pero faltaba la generacion de la estrella del agregado
por lo tanto se decidio crear el archivo EstrellasCargaDirecta el cual va realizando las insercciones en la base de datos a medida que va generando cada una de las tuplas para la dimension matricula y cancelacion que son las mas trabajables ya que se llenan a partir de la tabla auxiliar matricula_temp. Por lo tanto como este proceso ya ha sido trabajado y analizado anteriormente se explicara de que manera se realizo el proceso de creacion de la estrella agregado para matricula y cancelacion teniendo en cuenta los componentes que tiene.


Numero total de matriculas y cancelaciones de una asignatura que pertenecen a una escuela realizada por un programa academico en un fecha especifica durante una franja horaria.

1. Se tiene la tabla del agregado inicialmente vacia es decir 0 tuplas
2. Cuando aparece un registro de matricula/cancelacion/reactivacion en matricula temp se evalua si este ya esta en la tabla de agregado
usando una consulta que usa el operador count(*) de esta manera se sabe si ya hay coincidencias.
3. Dependiendo del resultado anterior pueden ocurrir dos cosas:
       *Si no hay coincidencias se procede a realizar un insert en el agregado teniendo en cuenta la llave de programa, asignatura,fecha 	 y franja horaria y dependiendo si realizo una matricula o una cancelacion para inicializar correctamente el valor en 1 para la  	primera matricula o cancelacion.

       *Si hay coincidencia se realiza una actualizacion en la tupla en la cual se suma uno al valor correspondiente a total  
	cancelaciones o al total de matriculas.
4. De esta manera tanto en el caso de matricula , reactivacion y cancelacion se tiene en cuenta este proceso para realizar la carga del agregado.

La carga de  las estrellas  fue probado realiazndo escrituras en consola y tardo aprox 2 horas en terminar el proceso.
