--crea tabla de hechos para estrella matricula,cancelacion y agregado_matricula_adicion_cancelacion

DROP TABLE matricula;
CREATE TABLE matricula
(
   fecha_key INTEGER,
   asignatura_key INTEGER,
   programa_key INTEGER,
   equipo_key INTEGER,
   estudiante_key INTEGER,
   demografia_key INTEGER,
   franja_horaria_key INTEGER,
   tabulado_key INTEGER,
   grupo INTEGER
);


DROP TABLE cancelacion;
CREATE TABLE cancelacion
(
   fecha_key INTEGER,
   asignatura_key INTEGER,
   programa_key INTEGER,
   equipo_key INTEGER,
   estudiante_key INTEGER,
   demografia_key INTEGER,
   franja_horaria_key INTEGER,
   tabulado_key INTEGER,
   grupo INTEGER
);

DROP TABLE agregado_matricula_adicion_cancelacion;
CREATE TABLE agregado_matricula_adicion_cancelacion
(
   fecha_key INTEGER,
   asignatura_key INTEGER,
   programa_key INTEGER,
   franja_horaria_key INTEGER,
   total_matriculas INTEGER,
   total_cancelaciones INTEGER,
   total_adiciones INTEGER
);

--ALTER TABLE matricula 
--ADD PRIMARY KEY(fecha_key, asignatura_key, programa_key, equipo_key,
--estudiante_key, demografia_key, franja_horaria_key, tabulado_key, grupo);

ALTER TABLE cancelacion
ADD PRIMARY KEY(fecha_key, asignatura_key, programa_key, equipo_key,
estudiante_key, demografia_key, franja_horaria_key, tabulado_key, grupo);

ALTER TABLE agregado_matricula_adicion_cancelacion
ADD PRIMARY KEY(fecha_key, asignatura_key, programa_key, franja_horaria_key);

--la siguiente tabla es temporal y es usada para llevar la cuenta de las cancelaciones y las matriculas y poder poblar
--la tabla de agregados
