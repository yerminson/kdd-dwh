
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DimensionEstudiante 
{
    public static void main(String[] args)
    {
        FachadaBD fachada = new FachadaBD();
        
        String sql_consultaEstudiante = "SELECT codigo, nombre, primer_apellido, segundo_apellido," +
                "genero, direccion, documento_identidad FROM estudiante_temp";
        String sql_consultaEmail = "SELECT email FROM email_temp ORDER BY RANDOM() LIMIT 1";
        String sql_consultaColegio = "SELECT colegio FROM colegio_temp ORDER BY RANDOM() LIMIT 1";
        
        String codigo;
        String nombre;
        String apellido;
        String apellido2;
        String documento_identidad;
        String direccion_residencia;
        String genero;
        String correo_electronico;
        String colegio;
        
        try
      {
         Connection conn= fachada.conectar();
         //System.out.println("consultando en la bd");
         Statement sentencia = conn.createStatement();
         Statement sentencia2 = conn.createStatement();
         Statement sentencia3 = conn.createStatement();
         ResultSet tabla = sentencia.executeQuery(sql_consultaEstudiante);
         ResultSet tablaEmail;
         ResultSet tablaColegio;
            
         while(tabla.next())
         {          
             codigo = tabla.getString(1);
             nombre = tabla.getString(2);
             apellido = tabla.getString(3);
             apellido2 = tabla.getString(4);
             genero = tabla.getString(5);
             direccion_residencia = tabla.getString(6);
             documento_identidad = tabla.getString(7);
             tablaEmail = sentencia2.executeQuery(sql_consultaEmail);
             tablaEmail.next();
             correo_electronico = tablaEmail.getString(1);
             tablaColegio = sentencia3.executeQuery(sql_consultaColegio);
             tablaColegio.next();
             colegio = tablaColegio.getString(1);
             
            System.out.println("INSERT INTO estudiante VALUES(DEFAULT,'" +
               codigo + "','" +
               nombre + "','" +
               apellido + "','" +
               apellido2 + "','" +
               documento_identidad + "','" +
               num(7) + "','" + //telefono
               direccion_residencia + "','" +
               genero + "','" +
               alea(1975,1994) + "-" + mes() + "-" + dia() + "','" + //fecha_nacimiento
               correo_electronico + "','" +
               colegio + "','" +
               alea(1990,2010) + "'," + //año_graduacion
               "DEFAULT" + "," + //validar_grado_flag
               "DEFAULT" + ",'" + //categoria
               "AC" + num(12) + "','" + //icfes_SNP
               "Antes del 2003" + "'," + //icfes_tipo
               alea(45,95) + "," + //icfes_biologia
               alea(45,95) + "," + //icfes_fisica
               alea(45,95) + "," + //icfes_lenguaje
               alea(45,95) + "," + //icfes_matematicas
               alea(45,95) + "," + //icfes_filosofia
               alea(45,95) + "," + //icfes_quimica
               alea(45,95) + "," + //icfes_geografia
               alea(45,95) + "," + //icfes_historia
               alea(45,95) + "," + //icfes_idioma
               alea(45,95) + //icfes_interdisciplinar
            ");");
         }
         fachada.cerrarConexion(conn);
      }catch(SQLException e)
      {
         System.out.println(e); 
      }
      catch(Exception e)
      {
         System.out.println(e); 
      }
    }
    
    static String num(int digitos)
   {
       String numd="";
       for(int i=0;i<digitos;i++)
       {
           numd += Integer.toString((int)(Math.random()*9)); 
       }
       return numd;
   }
    
   static String alea(int min, int max)
   {
       String a = Integer.toString((int)(min + (max-min)*Math.random()));
       return a;
   }
   
   static String dia()
   {
       String u = alea(0,3);
       if(!u.equals("3"))
       {
           u += alea(1,9);
       }else
       {
           u += alea(0,1);
       }
       return u;
   }
   
   static String mes()
   {
       String u = alea(0,1);
       if(u.equals("0"))
       {
           u += alea(1,9);
       }else
       {
           u += alea(0,1);
       }
       
       return u;
   }
}

