--dimension estudiante
DROP SEQUENCE estudiante_key_seq;
DROP TABLE estudiante;

CREATE SEQUENCE estudiante_key_seq
   INCREMENT 1
   MINVALUE 1
   START 1;

CREATE TABLE estudiante
(
   estudiante_key INTEGER NOT NULL DEFAULT nextval('estudiante_key_seq'),
   codigo CHAR(7),
   nombre VARCHAR(15),
   apellido VARCHAR(15),
   apellido2 VARCHAR(15),
   documento_identidad VARCHAR(10),
   telefono VARCHAR(7),
   direccion_residencia VARCHAR(50),
   genero CHAR(1),
   fecha_nacimiento DATE,
   correo_electronico VARCHAR(50),
   colegio VARCHAR(80),
   anio_graduacion INTEGER,
   validar_grado_flag BOOLEAN DEFAULT FALSE,
   categoria VARCHAR(10) DEFAULT 'Regular',
   icfes_SNP VARCHAR(14),
   icfes_tipo VARCHAR(20),
   icfes_biologia INTEGER,
   icfes_fisica INTEGER,
   icfes_lenguaje INTEGER,
   icfes_matematicas INTEGER,
   icfes_filosofia INTEGER,
   icfes_quimica INTEGER,
   icfes_geografia INTEGER,
   icfes_historia INTEGER,
   icfes_idioma INTEGER,
   icfes_interdisciplinar INTEGER
);
ALTER TABLE estudiante ADD CONSTRAINT estudiante_pk PRIMARY KEY (estudiante_key);
