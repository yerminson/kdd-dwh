DROP SEQUENCE fecha_key_seq;
CREATE SEQUENCE fecha_key_seq
	INCREMENT 1
	MINVALUE 1
	START 1;
DROP TABLE fecha;
CREATE TABLE fecha
(
   fecha_key INTEGER  NOT NULL DEFAULT nextval('fecha_key_seq'), 
   numero_dia   INTEGER, 
   numero_mes   INTEGER , 
   numero_anio   INTEGER ,
   nombre_dia   VARCHAR (10),
   nombre_mes   VARCHAR (10),
   semestre_anio INTEGER,
   periodo_academico VARCHAR (20), 
   numero_semana_semestre INTEGER,
   PRIMARY KEY (fecha_key)
);



