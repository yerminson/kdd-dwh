import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class DimensionFecha {

	static HashMap<Integer, String> dias;
	static HashMap<Integer, String> meses;

	public static void main(String[] args) throws IOException {

		inicializarHashMaps();

		boolean termine = false;
		// Fecha de Inicio = 01/01/2011
		int numero_año = 2010;
		int numero_mes = 1;
		int numero_dia = 1;
		Calendar calendario = new GregorianCalendar();
		calendario.set(numero_año, (numero_mes - 1), numero_dia);

		int numero_semana_año = calendario.get(Calendar.WEEK_OF_YEAR);
		int diaSemana = calendario.get(Calendar.DAY_OF_WEEK);
		int semestre_año = 0;

		String periodo_academico = "Enero";
		String nombre_mes = "Enero";
		String nombre_dia = dias.get(diaSemana);

		while (!termine) {

			if (numero_mes > 6) {

				semestre_año = 2;

			} else {

				semestre_año = 1;
			}

			if (numero_mes >= 2 && numero_mes <= 6) {

				periodo_academico = "Febrero-Julio";
				
			} else if (numero_mes >= 8 && numero_mes <= 12) {

				periodo_academico = "Agosto-Diciembre";

			} else if (numero_mes == 1) {

				periodo_academico = "Enero";
				
			} else if (numero_mes == 6) {

				periodo_academico = "Julio";

			}

			System.out.println("INSERT INTO fecha VALUES(DEFAULT," + numero_dia
					+ "," + numero_mes + "," + numero_año + "," + "'"+ nombre_dia + "'"
					+ "," +  "'"+ nombre_mes + "'"+ "," + semestre_año + ","
					+  "'"+periodo_academico + "'"+ "," + numero_semana_año + ");");
			if (numero_mes == 12 && numero_dia == 31 && numero_año == 2011) {
				
				termine = true;
			}
			calendario.add(Calendar.DAY_OF_MONTH, 1);
			

			diaSemana = calendario.get(Calendar.DAY_OF_WEEK);
			nombre_dia = dias.get(diaSemana);

			numero_dia = calendario.get(Calendar.DAY_OF_MONTH);
			numero_mes = calendario.get(Calendar.MONTH) + 1;
			nombre_mes = meses.get(numero_mes);
			numero_semana_año = calendario.get(Calendar.WEEK_OF_YEAR);
			numero_año = calendario.get(Calendar.YEAR);

			

		}
	}

	static void inicializarHashMaps() {

		dias = new HashMap<Integer, String>();

		dias.put(1, "Domingo");
		dias.put(2, "Lunes");
		dias.put(3, "Martes");
		dias.put(4, "Miercoles");
		dias.put(5, "Jueves");
		dias.put(6, "Viernes");
		dias.put(7, "Sabado");

		meses = new HashMap<Integer, String>();

		meses.put(1, "Enero");
		meses.put(2, "Febrero");
		meses.put(3, "Marzo");
		meses.put(4, "Abril");
		meses.put(5, "Mayo");
		meses.put(6, "Junio");
		meses.put(7, "Julio");
		meses.put(8, "Agosto");
		meses.put(9, "Septiembre");
		meses.put(10, "Octubre");
		meses.put(11, "Noviembre");
		meses.put(12, "Diciembre");

	}
}
