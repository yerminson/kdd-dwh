DROP SEQUENCE franja_horaria_key_seq;
CREATE SEQUENCE franja_horaria_key_seq
	INCREMENT 1
	MINVALUE 1
	START 1;
DROP TABLE franja_horaria;
CREATE TABLE franja_horaria
(
   franja_horaria_key INTEGER  NOT NULL DEFAULT nextval('franja_horaria_key_seq'), 
   hora_inicio  TIME, 
   hora_fin    TIME , 
   nombre_franja   varchar(10) ,
   PRIMARY KEY (franja_horaria_key)
);



