--dimension asignatura
DROP SEQUENCE asignatura_key_seq;
CREATE SEQUENCE asignatura_key_seq 
   INCREMENT 1 
   MINVALUE 1  
   START 1;

DROP TABLE asignatura;

CREATE TABLE asignatura
(
   asignatura_key INTEGER DEFAULT NEXTVAL('asignatura_key_seq'),
   codigo CHAR(10),
   nombre VARCHAR(110),
   creditos INTEGER,
   nivel VARCHAR(10),
   tipo CHAR(2),
   escuela_key CHAR(3)
   
);
ALTER TABLE asignatura ADD CONSTRAINT asignatura_key PRIMARY KEY (asignatura_key);
ALTER TABLE asignatura ADD CONSTRAINT escuela_asignatura_fk FOREIGN KEY (escuela_key) REFERENCES escuela(escuela_key);
