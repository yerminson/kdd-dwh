//package cargadatos;

import java.util.HashMap;
import java.util.Vector;

public class DimensionEquipo {

	static int N = 2700;
	static int M = 36;
	static int K = 200;
	static HashMap<String, Integer> direccionesMac = new HashMap<String, Integer>();
	static HashMap<String, Integer> numeroInventario = new HashMap<String, Integer>();
	static String[] macs = new String[N];
	static String[] numerosDeInventario = new String[N];

	static int[] edificios = { 380, 381, 382, 383, 384, 385, 386, 387, 388,
		301, 3170, 315, 316, 318, 320, 331, 333, 334, 335, 336, 338, 340,
		341, 344, 345, 346, 347, 348, 349, 350, 351, 352, 353, 354, 355,
		356 };

	static HashMap<Integer, Integer> computadoresEdificio = new HashMap<Integer, Integer>();

	static void generarCantidadComputadoresEdificio() {

		for (int i = 0; i < edificios.length; i++) {

			if (edificios[i] >= 333 && edificios[i] <= 356) {
				computadoresEdificio.put(edificios[i], 50);

			} else if (edificios[i] == 331) {

				computadoresEdificio.put(edificios[i], 200);

			} else {

				computadoresEdificio.put(edificios[i], 100);

			}

		}

	}

	static Vector<Integer> espaciosUsados = new Vector<Integer>();

	static int generarEspacio(int edificio) {

		int valor = (edificio * 1000 + (int) (Math.random() * 100));

		return valor;
	}

	static String[][] ips;

	static void generarIps() {

		ips = new String[M][K];

		for (int i = 0; i < M; i++) {

			for (int j = 0; j < computadoresEdificio.get(edificios[i]); j++) {

				ips[i][j] = "192.168." + (edificios[i]%300) + "." + (j + 2);

			}

		}

	}

	static void generarNumerosDeInventario() {

		for (int i = 0; i < N; i++) {

			int numero = (int) (Math.random() * 500000) + 400000;
			String cadena = Integer.toString(numero);

			if (numeroInventario.get(cadena) == null) {

				numeroInventario.put(cadena, numero);
				// System.out.println(fullMac+" - "+i);
				numerosDeInventario[i] = cadena;
			} else {

				i--;
			}

		}
	}

	static void generarDireccionesMac() {

		for (int i = 0; i < N; i++) {

			String mac = "" + Integer.toHexString(((int) (Math.random() * 15)))
					+ "" + Integer.toHexString(((int) (Math.random() * 15)))
					+ ":" + ""
					+ Integer.toHexString(((int) (Math.random() * 15))) + ""
					+ Integer.toHexString(((int) (Math.random() * 15))) + ":"
					+ "" + Integer.toHexString(((int) (Math.random() * 15)))
					+ "" + Integer.toHexString(((int) (Math.random() * 15)))
					+ ":" + ""
					+ Integer.toHexString(((int) (Math.random() * 15))) + ""
					+ Integer.toHexString(((int) (Math.random() * 15))) + ":"
					+ "" + Integer.toHexString(((int) (Math.random() * 15)))
					+ "" + Integer.toHexString(((int) (Math.random() * 15)))
					+ ":" + ""
					+ Integer.toHexString(((int) (Math.random() * 15))) + ""
					+ Integer.toHexString(((int) (Math.random() * 15)));

			if (direccionesMac.get(mac) == null) {
				String fullMac = "";
				for (int j = 0; j < mac.length(); j++) {

					fullMac += Character.toUpperCase(mac.charAt(j));
				}
				direccionesMac.put(mac, i);
				// System.out.println(fullMac+" - "+i);
				macs[i] = fullMac;
			} else {

				i--;
			}
		}

	}

	static HashMap<Integer, String> computadoresHP;
	static HashMap<Integer, String> computadoresLenovo;
	static HashMap<Integer, String> computadoresDell;
	static HashMap<Integer, String> computadoresMac;

	static HashMap<Integer, String> memoriaRam;
	static HashMap<Integer, String> discoDuro;
	static HashMap<Integer, String> procesador;
	static HashMap<Integer, String> tarjetaVideo;
	static HashMap<Integer, String> sistemaOperativo;

	static void inicializarAtributosComputador() {

		computadoresHP = new HashMap<Integer, String>();

		computadoresHP.put(1, "HP Pavilion serie a6300 Desktop PC");
		computadoresHP.put(2, "HP Pavilion serie a6700 Desktop PC");
		computadoresHP.put(3, "HP Pavilion serie a6100 Desktop PC");
		computadoresHP.put(4, "HP Pavilion serie a6200 Desktop PC");

		computadoresLenovo = new HashMap<Integer, String>();

		computadoresLenovo.put(1, "Lenovo ThinkCentre serie M91 Desktop PC");
		computadoresLenovo.put(2, "Lenovo ThinkCentre serie M91p Desktop PC");
		computadoresLenovo.put(3, "Lenovo ThinkCentre serie M81 Desktop PC");
		computadoresLenovo.put(4, "Lenovo ThinkCentre serie M71e Desktop PC");

		computadoresDell = new HashMap<Integer, String>();

		computadoresDell.put(1, "Dell OptiPlex serie 580");
		computadoresDell.put(2, "Dell OptiPlex serie 790");
		computadoresDell.put(3, "Dell OptiPlex serie 990");
		computadoresDell.put(4, "Dell OptiPlex serie 390");

		computadoresMac = new HashMap<Integer, String>();
		computadoresMac.put(1, "Mac Pro");

		memoriaRam = new HashMap<Integer, String>();

		memoriaRam.put(1, "4 GB DDR3");
		memoriaRam.put(2, "3 GB DDR3");
		memoriaRam.put(3, "2 GB DDR3");
		memoriaRam.put(4, "1 GB DDR3");

		discoDuro = new HashMap<Integer, String>();

		discoDuro.put(1, "80 GB");
		discoDuro.put(2, "160 GB");
		discoDuro.put(3, "320 GB");
		discoDuro.put(4, "500 GB");

		procesador = new HashMap<Integer, String>();

		procesador.put(1, "Procesador Intel® Core™ i7 ");
		procesador.put(2, "Procesador Intel® Core™ i5 ");
		procesador.put(3, "Procesadores AMD Athlon™ II");
		procesador.put(4, "Procesadores AMD Phenom™ II");

		tarjetaVideo = new HashMap<Integer, String>();

		tarjetaVideo.put(1, "GeForce GTX 580");
		tarjetaVideo.put(2, "GeForce GTX 580");
		tarjetaVideo.put(3, "ATI Radeon HD 4300");
		tarjetaVideo.put(4, "ATI Radeon HD 4550");

		sistemaOperativo = new HashMap<Integer, String>();

		sistemaOperativo.put(1, "Windows XP");
		sistemaOperativo.put(2, "Windows Vista");
		sistemaOperativo.put(3, "Windows 7");
		sistemaOperativo.put(4, "Ubuntu");
		sistemaOperativo.put(5, "Mac");

	}

	public static void main(String args[]) {

		generarDireccionesMac();
		generarCantidadComputadoresEdificio();
		inicializarAtributosComputador();

		generarNumerosDeInventario();
		generarIps();

		String mac = "";
		String ip = "";
		String espacio = "";
		String edificio = "";
		String marca = "";
		String modelo = "";
		String inventario = "";
		String capacidad_ram = "";
		String capacidad_disco = "";
		String tipo_procesador = "";
		String tarjeta_video = "";
		String sistema_operativo = "";

		// Carga de dimension equipo.
		int contador = 0;

		
		for (int i = 0; i < edificios.length; i++) {

			int cantidadComputadores = computadoresEdificio.get(edificios[i]);
			int factor = cantidadComputadores / 4;

			boolean repetido = false;
			
			for (int j = 0; j < cantidadComputadores; j++) {
				// System.out.println(contador);
				mac = macs[contador];
				ip = ips[i][j];
				
				

				if (j <= (factor)) {

					int numeroEspacio = generarEspacio(1);
					if (!espaciosUsados.contains(numeroEspacio)) {

						espacio = Integer.toString(numeroEspacio);
						espaciosUsados.add(numeroEspacio);
						repetido = false;
					} else {

						repetido = true;
						j--;
						contador--;

					}

				} else if (j <= (factor * 2)) {

					int numeroEspacio = generarEspacio(2);
					if (!espaciosUsados.contains(numeroEspacio)) {

						espacio = Integer.toString(numeroEspacio);
						espaciosUsados.add(numeroEspacio);
						repetido = false;
					} else {

						repetido = true;
						j--;
						contador--;

					}
				} else if (j <= (factor * 3)) {
					int numeroEspacio = generarEspacio(3);
					if (!espaciosUsados.contains(numeroEspacio)) {

						espacio = Integer.toString(numeroEspacio);
						espaciosUsados.add(numeroEspacio);
						repetido = false;
					} else {

						repetido = true;
						j--;
						contador--;

					}

				} else {
					int numeroEspacio = generarEspacio(4);
					if (!espaciosUsados.contains(numeroEspacio)) {

						espacio = Integer.toString(numeroEspacio);
						espaciosUsados.add(numeroEspacio);
						repetido = false;
					} else {

						repetido = true;
						j--;
						contador--;

					}

				}

				if(!repetido){
				edificio = Integer.toString(edificios[i]);
				int valorMarca = (int) ((Math.random() * 4) + 1);
				int valorModelo = (int) ((Math.random() * 4) + 1);

				if (valorMarca == 1) {// HP

					marca = "HP";
					modelo = computadoresHP.get(valorModelo);

				} else if (valorMarca == 2) {// Lenovo
					marca = "Lenovo";
					modelo = computadoresLenovo.get(valorModelo);

				} else if (valorMarca == 3) {// Dell
					marca = "Dell";
					modelo = computadoresDell.get(valorModelo);

				} else if (valorMarca == 4) {// Mac
					marca = "Mac";
					modelo = computadoresMac.get(1);
					sistema_operativo = sistemaOperativo.get(5);

				}

				inventario = numerosDeInventario[contador];

				int valorMemoria = (int) ((Math.random() * 4) + 1);
				int valorDiscoDuro = (int) ((Math.random() * 4) + 1);
				int valorProcesador = (int) ((Math.random() * 4) + 1);
				int valorTarjetaVideo = (int) ((Math.random() * 4) + 1);
				int valorSistemaOperativo = (int) ((Math.random() * 4) + 1);

				capacidad_ram = memoriaRam.get(valorMemoria);
				capacidad_disco = discoDuro.get(valorDiscoDuro);
				tipo_procesador = procesador.get(valorProcesador);
				tarjeta_video = tarjetaVideo.get(valorTarjetaVideo);
				if (valorMarca != 4)
					sistema_operativo = sistemaOperativo
					.get(valorSistemaOperativo);

				
				System.out.println("INSERT INTO equipo VALUES(DEFAULT," + "'"
						+ mac + "','" + ip + "','" + espacio + "','" + ""
						+ edificio + "','" + marca + "','" + modelo + "','"
						+ inventario + "','" + capacidad_ram + "','"
						+ "" + capacidad_disco + "','" + tipo_procesador
						+ "','" + tarjeta_video + "','" + sistema_operativo
						+ "');");

				}
				
				contador++;
				
			}

			espaciosUsados.clear();

		}
		for(int i=0; i< N; i++){
			
			mac = "00:00:00:00:00:00";
			ip = "100.100.100.100";
			espacio = "12";
			edificio = "10";
			marca ="generica";
			modelo ="generico";
			inventario = "00000000";
			capacidad_disco = "Desco.";
			capacidad_ram = "Desco.";
			tipo_procesador ="Desco.";
			tarjeta_video = "Desco.";
			sistema_operativo = "Desco.";
			
			
			System.out.println("INSERT INTO equipo VALUES(DEFAULT," + "'"
					+ mac + "','" + ip + "','" + espacio + "','" + ""
					+ edificio + "','" + marca + "','" + modelo + "','"
					+ inventario + "','" + capacidad_ram + "','"
					+ "" + capacidad_disco + "','" + tipo_procesador
					+ "','" + tarjeta_video + "','" + sistema_operativo
					+ "');");

			
		}
		
			
		
	}

	
}


