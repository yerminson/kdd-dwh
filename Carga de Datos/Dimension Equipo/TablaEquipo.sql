--dimension equipo
DROP SEQUENCE equipo_key_seq ;
CREATE SEQUENCE equipo_key_seq 
   INCREMENT 1 
   MINVALUE 1  
   START 1;

DROP TABLE equipo;

CREATE TABLE equipo
(
   equipo_key INTEGER DEFAULT NEXTVAL('equipo_key_seq'),
   mac CHAR(17),
   ip VARCHAR(15),
   espacio CHAR(5),
   edificio CHAR(4),
   marca VARCHAR(10),
   modelo VARCHAR(100),
   inventario VARCHAR(10),
   capacidad_ram VARCHAR(10),
   capacidad_disco VARCHAR(10),
   procesador VARCHAR(50),
   tarjeta_video VARCHAR(50),
   sistema_operativo VARCHAR(20)
 
   
);
ALTER TABLE equipo ADD CONSTRAINT equipo_key PRIMARY KEY (equipo_key);

