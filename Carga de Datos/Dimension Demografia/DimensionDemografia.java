
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;

public class DimensionDemografia 
{
    public static void main(String[] args)
    {
        FachadaBD fachada = new FachadaBD();
        
        HashMap<Integer, String> estadoCivil;
        estadoCivil = new HashMap<Integer, String>();
        estadoCivil.put(1, "Soltero/a");
        estadoCivil.put(2, "Casado/a");
        estadoCivil.put(3, "Union Libre");
        estadoCivil.put(4, "Viudo/a");
        estadoCivil.put(5, "Divorciado/a");
        
        String sql_consultaBarrio = "SELECT nombre,estrato,comuna FROM barrio_temp";
        
        String barrio;
        String comuna;
        String estrato;
        
        try
        {
            Connection conn= fachada.conectar();
            //System.out.println("consultando en la bd");
            Statement sentencia = conn.createStatement();
            ResultSet tabla = sentencia.executeQuery(sql_consultaBarrio);
            
            while(tabla.next())
            {
                barrio = tabla.getString(1);
                estrato = tabla.getString(2);
                comuna = tabla.getString(3);
                
                for(int i=1;i<=5;i++)
                {
                    System.out.println("INSERT INTO demografia VALUES(" +
                            "DEFAULT," +
                            "'CC','" +
                            estadoCivil.get(i) + "'," +
                            "'Cali','" +
                            barrio + "','" +
                            comuna + "'," +
                            estrato + "," +
                            alea(0,4) + //numero hermanos
                            ")");
                }
            }
            fachada.cerrarConexion(conn);
        }catch(SQLException e)
        {
            System.out.println(e); 
        }
        catch(Exception e)
        {
            System.out.println(e); 
        }
    }
    
    static String alea(int min, int max)
   {
       String a = Integer.toString((int)(min + (max-min)*Math.random()));
       return a;
   }
}

