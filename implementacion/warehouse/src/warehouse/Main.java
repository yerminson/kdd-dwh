/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package warehouse;

import com.nilo.plaf.nimrod.NimRODLookAndFeel;
import com.nilo.plaf.nimrod.NimRODTheme;
import java.awt.Color;
import net.sf.jasperreports.engine.JRException;
import javax.swing.UIManager;


/**
 *
 * @author andrea
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws JRException {
        // TODO code application logic here
        try
			{
				NimRODTheme nt = new NimRODTheme(new Color(35, 210, 255), Color.LIGHT_GRAY);
				NimRODLookAndFeel NimRODLF = new NimRODLookAndFeel();
				NimRODLookAndFeel.setCurrentTheme(nt);
				UIManager.setLookAndFeel( NimRODLF);
			}
			catch (Exception e){e.printStackTrace();}
        mainWindow ventana = new mainWindow();
        ventana.setResizable(false);
        ventana.setVisible(true);

        /*ControladorReporte controlador = new ControladorReporte();
        JasperPrint print = controlador.reporteUsoDias("total_matriculas", 10);
        ventana.modificarVistaParametrizadas(print);*/
        /*DaoReporte dao = new DaoReporte();
        JTable table = dao.consultaSQL("select * from franja_horaria");
        ventana.modificarVIstaSQL(table);*/


    }

}
