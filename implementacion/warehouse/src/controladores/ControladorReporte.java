/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controladores;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javax.swing.JTable;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRTableModelDataSource;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;
import daos.DaoReporte;
import java.io.File;
import java.sql.Connection;
import net.sf.jasperreports.engine.JasperCompileManager;


/**
 *
 * @author andrea
 */
public class ControladorReporte {

    private DaoReporte daoReportes;

    public ControladorReporte()
    {
        daoReportes = new DaoReporte();
    }

    public JTable reporteSQL(String consultaSQL)
    {
        return daoReportes.consultaSQL(consultaSQL);
    }

    public JTable reporteNoUsoEquipos(String opcion)
    {
        return daoReportes.consultarNoUsoEquipos(opcion);
    }
    public JasperPrint reporteUsoDias(String opcion, String orden, int limit) throws JRException {

        String consultaSQL = daoReportes.consultaUsoDias(opcion, orden, limit);

        Map<String, String> parametros = new HashMap<String, String>();
        parametros.put("nombreSerie", opcion);
        parametros.put("query", consultaSQL);


        JasperReport reporte;
        if(opcion.equals("total_matriculas") || opcion.equals("total_cancelaciones"))
            reporte = JasperCompileManager.compileReport("reportes/reporteUsoDimension.jrxml");
        else
            reporte = JasperCompileManager.compileReport("reportes/reporteUsoDimensionComparacion.jrxml");
        Connection conn= daoReportes.conectar();
        JasperPrint jasperPrint = JasperFillManager.fillReport(reporte, parametros, conn);
        daoReportes.cerrarConexion(conn);

        parametros = null;
        reporte = null;
        conn=null;
        return jasperPrint;
    }

    public JasperPrint reporteUsoDimension(String opcion, String atributo, String orden, String nombreAtributo,
            String tablaAtributo, int limit) throws JRException {
        String consultaSQL = daoReportes.consultaComportamiento(opcion, atributo, orden, nombreAtributo,
                tablaAtributo, limit);

        Map<String, String> parametros = new HashMap<String, String>();
        parametros.put("nombreSerie", opcion);
        parametros.put("query", consultaSQL);


        JasperReport reporte;
        if(opcion.equals("total_matriculas") || opcion.equals("total_cancelaciones"))
            reporte = JasperCompileManager.compileReport("reportes/reporteUsoDimension.jrxml");
        else
            reporte = JasperCompileManager.compileReport("reportes/reporteUsoDimensionComparacion.jrxml");
        Connection conn= daoReportes.conectar();
        JasperPrint jasperPrint = JasperFillManager.fillReport(reporte, parametros, conn);
        daoReportes.cerrarConexion(conn);

        parametros = null;
        reporte = null;
        conn=null;
        return jasperPrint;
    }

    public JasperPrint reporteUsoFecha(String opcion, String orden, int limit) throws JRException {
        String consultaSQL = daoReportes.consultaFecha(opcion, orden, limit);

        Map<String, String> parametros = new HashMap<String, String>();
        parametros.put("nombreSerie", opcion);
        parametros.put("query", consultaSQL);

        JasperReport reporte;
        if(opcion.equals("total_matriculas") || opcion.equals("total_cancelaciones"))
            reporte = JasperCompileManager.compileReport("reportes/reporteUsoDimension.jrxml");
        else
            reporte = JasperCompileManager.compileReport("reportes/reporteUsoDimensionComparacion.jrxml");
        Connection conn= daoReportes.conectar();
        JasperPrint jasperPrint = JasperFillManager.fillReport(reporte, parametros, conn);
        daoReportes.cerrarConexion(conn);

        parametros = null;
        reporte = null;
        conn=null;
        return jasperPrint;
    }

    public JasperPrint reporteAsignaturas(String asignatura1, String asignatura2, String ordenPor, String opcion) throws JRException
    {
        String consultaSQL= daoReportes.consultarAsignaturas( asignatura1,  asignatura2,  ordenPor, opcion);

        Map<String, String> parametros = new HashMap<String, String>();
        parametros.put("query", consultaSQL);
        boolean dos= !asignatura2.equals("");
        if(dos)
        {
            parametros.put("asignatura2", asignatura2);
        }
        parametros.put("asignatura1", asignatura1);

        JasperReport reporte;
        if(dos)
            reporte = JasperCompileManager.compileReport("reportes/reporteAsignaturas.jrxml");
        else
            reporte = JasperCompileManager.compileReport("reportes/reporteAsignatura.jrxml");
        Connection conn= daoReportes.conectar();
        JasperPrint jasperPrint = JasperFillManager.fillReport(reporte, parametros, conn);
        daoReportes.cerrarConexion(conn);

        parametros = null;
        reporte = null;
        conn=null;
        return jasperPrint;
        
    }

    public boolean validarAsignatura(String asig)
    {
        boolean b= daoReportes.validarAsignatura(asig);
        return b;
    }

    public boolean validarPrograma(String pro)
    {
        boolean b = daoReportes.validarPrograma(pro);
        return b;
    }

    public JasperPrint reporteEquipoUsoPor(String opcion, String orden) throws JRException
    {
        String consultaSQL= daoReportes.consultarEquiporPor(opcion, orden);

        Map<String, String> parametros = new HashMap<String, String>();
        parametros.put("query", consultaSQL);

        JasperReport reporte;
        reporte = JasperCompileManager.compileReport("reportes/reporteUsoEquipos.jrxml");
        Connection conn= daoReportes.conectar();
        JasperPrint jasperPrint = JasperFillManager.fillReport(reporte, parametros, conn);
        daoReportes.cerrarConexion(conn);

        parametros = null;
        reporte = null;
        conn=null;
        return jasperPrint;
    }

    public JasperPrint reporteEquipoPrograma(String opcion, String programa, String orden) throws JRException
    {
        String consultaSQL= daoReportes.consultarEquipoPrograma(opcion, programa, orden);

        Map<String, String> parametros = new HashMap<String, String>();
        parametros.put("query", consultaSQL);

        JasperReport reporte;
        reporte = JasperCompileManager.compileReport("reportes/reporteUsoEquipos.jrxml");
        Connection conn= daoReportes.conectar();
        JasperPrint jasperPrint = JasperFillManager.fillReport(reporte, parametros, conn);
        daoReportes.cerrarConexion(conn);

        parametros = null;
        reporte = null;
        conn=null;
        return jasperPrint;
    }
}
