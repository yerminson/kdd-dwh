/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package daos;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import utilidades.FachadaBD;

/**
 *
 * @author andrea
 */
public class DaoReporte {

    FachadaBD fachada;

    public DaoReporte() {
        fachada = new FachadaBD();
    }

    public Connection conectar() {
        return this.fachada.conectar();
    }

    public void cerrarConexion(Connection conn) {
        this.fachada.cerrarConexion(conn);
    }

    public Vector consultarEsquema() {
        Vector dwh = new Vector();
        String consultaSql = "select table_name from information_schema.tables where table_schema='public' "
                + " order by table_name ";

        dwh = procesarEsquema(consultaSql);
        return dwh;
    }

    public Vector consultarEsquemaTabla(String table) {
        Vector tableS = new Vector();
        String consultaSql = "select column_name from information_schema.columns "
                + " where table_name= '" + table + "'";

        tableS = procesarEsquema(consultaSql);
        return tableS;
    }

    public Vector procesarEsquema(String consultaSql) {
        Vector data = new Vector();
        try {
            Connection conn = fachada.conectar();
            Statement sentencia = conn.createStatement();
            ResultSet resultado = sentencia.executeQuery(consultaSql);

            while (resultado.next()) {
                data.add(resultado.getString(1));

            }
            fachada.cerrarConexion(conn);
            conn = null;
            //fachada = null;
            sentencia = null;
            resultado = null;
        } catch (SQLException e) {
            System.out.println(e);
        } catch (Exception e) {
            System.out.println(e);
        }
        return data;
    }

    public String consultaUsoDias(String opcion, String orden, int limit) {
        String limite = "";
        if (limit != 0) {
            limite = " limit " + limit;
        }

        String consultaSql;
        if (opcion.equals("total_matriculas") || opcion.equals("total_cancelaciones")) {
            consultaSql = "select datos.nombre_dia as nombre , sum(totales.total) as total from "
                    + "(select sum( " + opcion + " ) as total, fecha_key "
                    + "from agregado_matricula_adicion_cancelacion "
                    + "group by fecha_key "
                    + "having sum( " + opcion + ") >0) as totales "
                    + "natural join "
                    + "(select fecha_key, nombre_dia  from fecha ) as datos group by datos.nombre_dia "
                    + " order by sum(totales.total) " + orden + limite + " ;";
        } else {
            consultaSql = "select datos.nombre_dia as nombre , sum(totales.total) as total, "
                    + " sum(totales.totalM) as totalMatricula, sum(totales.totalC) as totalCancelacion from "
                    + "(select sum( " + opcion + " ) as total, sum(total_matriculas) as totalM, fecha_key, "
                    + " sum(total_cancelaciones) as totalC "
                    + "from agregado_matricula_adicion_cancelacion "
                    + "group by fecha_key "
                    + "having sum( " + opcion + ") >0) as totales "
                    + "natural join "
                    + "(select fecha_key, nombre_dia  from fecha ) as datos group by datos.nombre_dia "
                    + " order by sum(totales.total) " + orden + limite + " ;";
        }


        return consultaSql;
    }

    public String consultaFecha(String opcion, String orden, int limit) {
        String limite = "";
        if (limit != 0) {
            limite = " limit " + limit;
        }

        String consultaSql;
        if (opcion.equals("total_matriculas") || opcion.equals("total_cancelaciones")) {
            consultaSql = "select datos.numero_dia || '/' ||  datos.numero_mes || '/' ||"
                    + " datos.numero_anio as nombre, totales.total as total from "
                    + " (select sum( " + opcion + " ) as total, fecha_key"
                    + " from agregado_matricula_adicion_cancelacion "
                    + "group by fecha_key having sum( " + opcion + " ) > 0) as totales"
                    + " natural join "
                    + " (select fecha_key, numero_dia, numero_mes, numero_anio  from fecha )"
                    + "as datos order by totales.total " + orden + limite + " ;";
        } else {
            consultaSql = "select datos.numero_dia || '/' ||  datos.numero_mes || '/' ||"
                    + " datos.numero_anio as nombre, totales.total as total, totales.totalM "
                    + " as totalMatricula, totales.totalC as totalCancelacion from "
                    + " (select sum( " + opcion + " ) as total, sum(total_matriculas) as totalM, "
                    + " sum(total_cancelaciones) as totalC, fecha_key "
                    + " from agregado_matricula_adicion_cancelacion "
                    + "group by fecha_key having sum( " + opcion + " ) > 0) as totales"
                    + " natural join "
                    + " (select fecha_key, numero_dia, numero_mes, numero_anio  from fecha )"
                    + "as datos order by totales.total " + orden + limite + " ;";
        }



        return consultaSql;
    }

    public String consultaComportamiento(String opcion, String atributo,
            String orden, String nombreAtributo, String tablaAtributo, int limit) {
        String limite = "";
        if (limit != 0) {
            limite = " limit " + limit;
        }

        String consultaSql;

        if (opcion.equals("total_matriculas") || opcion.equals("total_cancelaciones")) {
            consultaSql = "select datos.nombre, totales.total from"
                    + "(select sum(" + opcion + " ) as total, " + atributo
                    + " from agregado_matricula_adicion_cancelacion "
                    + "group by " + atributo
                    + " having sum( " + opcion + " ) > 0 ) as totales"
                    + " natural join"
                    + " (select " + atributo + " ," + nombreAtributo + " as nombre "
                    + "from " + tablaAtributo + " ) as datos "
                    + "order by totales.total " + orden + limite + " ;";
        } else {
            consultaSql = "select datos.nombre as nombre , totales.total as total, totales.totalC "
                    + " as totalCancelacion, totales.totalM as totalMatricula from"
                    + "(select sum(" + opcion + " ) as total, sum(total_matriculas) as totalM, "
                    + " sum(total_cancelaciones) as totalC, " + atributo
                    + " from agregado_matricula_adicion_cancelacion "
                    + "group by " + atributo
                    + " having sum( " + opcion + " ) > 0 ) as totales"
                    + " natural join"
                    + " (select " + atributo + " ," + nombreAtributo + " as nombre "
                    + "from " + tablaAtributo + " ) as datos "
                    + "order by totales.total " + orden + limite + " ;";

        }

        return consultaSql;
    }

    public JTable consultaSQL(String consultaSql) {
        JTable data = new JTable();
        DefaultTableModel modelo = new DefaultTableModel();

        try {
            Connection conn = fachada.conectar();
            Statement sentencia = conn.createStatement();
            ResultSet resultado = sentencia.executeQuery(consultaSql);
            ResultSetMetaData metaData = resultado.getMetaData();

            int col = metaData.getColumnCount();


            for (int i = 0; i < col; i++) {
                modelo.addColumn(metaData.getColumnName(i + 1));
                System.out.println(metaData.getColumnTypeName(i + 1));

            }

            while (resultado.next()) {
                Vector<Object> row = new Vector<Object>(0, 1);
                for (int i = 0; i < col; i++) {
                    row.add(resultado.getObject(i + 1));
                }

                modelo.addRow(row);

            }
            data.setModel(modelo);
            fachada.cerrarConexion(conn);
            conn = null;
            //fachada = null;
            sentencia = null;
            resultado = null;
            metaData = null;
        } catch (SQLException e) {
            modelo.addColumn("ERROR");
            Vector row = new Vector();
            row.add(" " + e);
            modelo.addRow(row);
            data.setModel(modelo);

            System.out.println(e);
        } catch (Exception e) {
            System.out.println(e);
        }

        return data;
    }

    public boolean validarAsignatura(String asignatura) {
        String consultaSql = "select nombre from asignatura where nombre= '"
                + asignatura + "';";
        Vector b = procesarEsquema(consultaSql);
        System.out.println(b);
        return b.size() != 0 ? true : false;
    }

    public boolean validarPrograma(String programa) {
        String consultaSql = "select nombre from programa where nombre= '"
                + programa + "';";
        Vector b = procesarEsquema(consultaSql);
        return b.size() != 0 ? true : false;
    }

    public String consultarAsignaturas(String asignatura1, String asignatura2, String ordenPor, String opcion) {
        boolean dos = !asignatura2.equals("");
        boolean eBool = ordenPor.equals("escuela");
        String select = "", asigDos = "", selectPrograma = "", conEscuela = "", groupBy = "";

        if (!eBool && dos) {
            select = " nombre, suma, suma2 ";
        }
        if (!eBool && !dos) {
            select = " nombre, suma ";
        }
        if (eBool && dos) {
            select = " sum(a.suma)as suma, sum(d.suma2)as suma2, nombreE as nombre ";
        }
        if (eBool && !dos) {
            select = " sum(a.suma)as suma, nombreE as nombre ";
        }
        if (dos) {
            asigDos = " (select sum( " + opcion + ") as suma2, programa_key from agregado_matricula_adicion_cancelacion "
                    + " natural join (select asignatura_key from asignatura where nombre='" + asignatura2
                    + "')as k group by programa_key) as d natural join ";
        }
        if (eBool) {
            selectPrograma = " programa_key, nombreE ";
            conEscuela = " natural join (select nombre as nombreE, escuela_key from escuela) as b ";
            groupBy = " group by nombre ";
        } else {
            selectPrograma = "programa_key, nombre ";
        }

        String consultaSQL = "select " + select + "from (select sum( " + opcion + ") as suma, programa_key from "
                + " agregado_matricula_adicion_cancelacion natural join (select asignatura_key from  "
                + " asignatura where nombre='" + asignatura1 + "') as j  group by programa_key) as a "
                + " natural join " + asigDos + "(select " + selectPrograma + " from programa "
                + conEscuela + " ) as t" + groupBy + ";";

        System.out.println(consultaSQL);
        return consultaSQL;

    }

    public JTable consultarNoUsoEquipos(String opcion) {
        String tabla = "";
        String consultaSql;

        if (opcion.equals("total_cancelaciones") || opcion.equals("total_matriculas")) {
            if (opcion.equals("total_cancelaciones")) {
                tabla = " cancelacion ";
            } else {
                tabla = " matricula ";
            }

            consultaSql = "select p.programa_key, p.nombre from programa as p where "
                    + " programa_key not in (select programa_key from " + tabla
                    + " natural join (select equipo_key from equipo where edificio!='10') as e ) "
                    + " and programa_key in (select distinct(programa_key) from " + tabla + " )";

        }else
        {
            consultaSql= "select * from (select p.programa_key, p.nombre from programa as p where "
                    + " programa_key not in (select programa_key from matricula natural join "
                    + " (select equipo_key from equipo where edificio!='10') as e ) and programa_key in "
                    + " (select distinct(programa_key) from matricula)) as matri natural join "
                    + " (select p.programa_key, p.nombre from programa as p where programa_key not in "
                    + " (select programa_key from cancelacion natural join (select equipo_key from equipo "
                    + " where edificio!='10' ) as eq) and programa_key in (select distinct(programa_key) "
                    + " from cancelacion)) as cance";
        }


        System.out.println(consultaSql);
        JTable data = new JTable();
        DefaultTableModel modelo = new DefaultTableModel();

        try {
            Connection conn = fachada.conectar();
            Statement sentencia = conn.createStatement();
            ResultSet resultado = sentencia.executeQuery(consultaSql);
            ResultSetMetaData metaData = resultado.getMetaData();

            int col = metaData.getColumnCount();


            for (int i = 0; i < col; i++) {
                modelo.addColumn(metaData.getColumnName(i + 1));
                System.out.println(metaData.getColumnTypeName(i + 1));

            }

            while (resultado.next()) {
                Vector<Object> row = new Vector<Object>(0, 1);
                for (int i = 0; i < col; i++) {
                    row.add(resultado.getObject(i + 1));
                }

                modelo.addRow(row);

            }
            data.setModel(modelo);
            fachada.cerrarConexion(conn);
            conn = null;
            //fachada = null;
            sentencia = null;
            resultado = null;
            metaData = null;
        } catch (SQLException e) {
            modelo.addColumn("ERROR");
            Vector row = new Vector();
            row.add(" " + e);
            modelo.addRow(row);
            data.setModel(modelo);

            System.out.println(e);
        } catch (Exception e) {
            System.out.println(e);
        }

        return data;

    }

    public String consultarEquiporPor(String opcion, String orden)
    {
        String tabla="";
        if(opcion.equals("total_cancelaciones"))
            tabla=" cancelacion ";
        else
            tabla=" matricula ";

        String select="", ordenConsulta="", finConsulta="";
        if(orden.equals("programa"))
        {
            select=" nombre, u.equiposUnivalle , e.equiposExterior ";
            ordenConsulta=" programa_key ";
            finConsulta="(select nombre, programa_key from programa) as p order by nombre;";
        }else
        {
            select=" nombre , sum(u.equiposUnivalle) as equiposUnivalle , "
                    +" sum(e.equiposExterior) as equiposExterior ";
            
        }

        if(orden.equals("escuela"))
        {
            ordenConsulta=" programa_key ";
            finConsulta="(select nombreE as nombre, programa_key from programa natural join "
                    + " (select nombre as nombreE, escuela_key from escuela) as escu ) as p group by "
                    + " nombre order by nombre;";
        } else if(orden.equals("franja horaria"))
        {
            ordenConsulta=" franja_horaria_key ";
            finConsulta="(select nombre_franja as nombre, franja_horaria_key from franja_horaria ) "
                    + " as f group by nombre order by nombre;";
        }else if (orden.equals("dia de la semana"))
        {
            ordenConsulta=" fecha_key ";
            finConsulta="(select nombre_dia as nombre , fecha_key from fecha ) as f group by nombre order by nombre;";
        }
            

        String consultaSql="select " + select + " from (select " + ordenConsulta
                + " , count(*) as equiposUnivalle from " + tabla+" natural join "
                + " (select equipo_key from equipo where edificio!='10') as eq group by "
                + ordenConsulta + " ) as u natural join (select "+ ordenConsulta 
                + " , count(*) as equiposExterior from " + tabla +" natural join (select "
                + " equipo_key from equipo where edificio ='10') as eqq group by "
                + ordenConsulta +" ) as e natural join " + finConsulta ;

       // System.out.println(consultaSql);
        return consultaSql;

    }

    public String consultarEquipoPrograma(String opcion, String programa, String orden)
    {

        String tabla="", ordenConsulta="", finConsulta="";
        if(opcion.equals("total_cancelaciones"))
            tabla=" cancelacion ";
        else
            tabla=" matricula ";
        if(orden.equals("franja horaria"))
        {
            ordenConsulta=" franja_horaria_key ";
            finConsulta=" (select nombre_franja as nombre, franja_horaria_key from franja_horaria) ";
        }
            
        else
        {
            ordenConsulta=" fecha_key ";
            finConsulta=" (select nombre_dia as nombre, fecha_key from fecha) ";
        }
            

        String consultaSql="select nombre, sum(u.equiposUnivalle) as equiposUnivalle , "
                + " sum(e.equiposExterior) as equiposExterior from (select "
                + ordenConsulta + " , count(*) as equiposUnivalle from " + tabla + " natural join "
                + " (select programa_key from programa where nombre='QUIMICA') as p natural join "
                + " (select equipo_key from equipo where edificio!='10') as eq group by "
                + ordenConsulta + " ) as u natural join (select " + ordenConsulta + ", count(*) as "
                + " equiposExterior from " + tabla +" natural join (select programa_key from programa "
                + " where nombre='QUIMICA') as pp natural join (select equipo_key from equipo where "
                + " edificio ='10') as eqq group by " + ordenConsulta +" ) as e natural join "
                + finConsulta + " as f group by nombre order by nombre;";

        //System.out.print(consultaSql);
        return consultaSql;
    }


}
