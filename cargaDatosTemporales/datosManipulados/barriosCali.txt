0101*2*Terrón Colorado*1
0102*1*Vista Hermosa*1
0196*1*Sector Patio Bonito*1
0199*1*Aguacatal*1
0201*6*Santa Rita*2
0202*6*Santa Teresita*2
0203*6*Arboledas*2
0204*6*Normandía*2
0205*5*Juanambú*2
0206*5*Centenario*2
0207*4*Granada*2
0208*5*Versalles*2
0209*4*San Vicente*2
0210*5*Santa Mónica *2
0211*5*Prados del Norte*2
0212*5*La Flora*2
0213*4*La Campiña*2
0214*3*La Paz*2
0215*5*El Bosque*2
0216*6*Menga*2
0217*4*Ciudad Los Alamos*2
0218*4*Chipichape*2
0219*3*Brisas de los Alamos*2
0293*5*Urbanización La Merced*2
0294*4*Vipasa*2
0295*5*Urbanización La Flora*2
0296*1*Altos de Menga*2
0297*1*Sector Altos Normandía - Bataclán *2
0298*5*Area en desarrollo - Parque del Amor*2
0301*3*El Nacional*3
0302*5*El Peñón*3
0303*3*San Antonio*3
0304*3*San Cayetano*3
0305*3*Los Libertadores*3
0306*3*San Juan Bosco*3
0307*3*Santa Rosa*3
0308*3*La Merced*3
0309*1*San Pascual*3
0310*3*El Calvario*3
0311*3*San Pedro*3
0312*2*San Nicolas*3
0313*3*El Hoyo*3
0314*2*El Piloto*3
0315*3*Navarro - La Chanca*3
0316*2*Acueducto San Antonio*3
0401*2*Jorge Isaacs*4
0402*2*Santander*4
0403*2*Porvenir*4
0404*3*Las Delicias*4
0405*3*Manzanares*4
0406*3*Salomia*4
0407*2*Fátima*4
0408*2*Sultana - Berlín - San Francisco *4
0410*3*Popular*4
0411*3*Ignacio Rengifo*4
0412*2*Guillermo Valencia*4
0413*2*La Isla*4
0414*2*Marco Fidel Suárez*4
0415*2*Evaristo García*4
0416*2*La Esmeralda*4
0417*2*Bolivariano*4
0418*2*Barrio Olaya Herrera*4
0419*3*Unid. Res. Bueno Madrid*4
0420*3*Flora Industrial*4
0421*3*Calima*4
0497*3*La Alianza*4
0501*3*El Sena*5
0502*3*Los Andes*5
0503*3*Los Guayacanes*5
0504*3*Chiminangos Segunda Etapa*5
0505*3*Chiminangos Primera Etapa*5
0506*3*Metropolitano del Norte*5
0594*3*Los Parques - Barranquilla*5
0595*4*Villa del Sol*5
0596*3*Paseo de Los Almendros*5
0597*3*Los Andes B - La Riviera*5
0598*3*Torres de Comfandi*5
0599*3*Villa del Prado - El Guabito*5
0601*2*San Luís*6
0602*2*Jorge Eliecer Gaitán*6
0603*2*Paso del Comercio*6
0604*3*Los Alcazares*6
0605*2*Petecuy Primera Etapa*6
0606*2*Petecuy Segunda Etapa*6
0607*2*La Rivera I*6
0608*3*Los Guaduales*6
0609*2*Petecuy Tercera Etapa*6
0610*2*Ciudadela Floralia*6
0695*3*Fonaviemcali*6
0696*2*San Luís II*6
0697*2*Urbanización Calimio*6
0698*2*Sector Puente del Comercio*6
0701*3*Alfonso López 1° Etapa*7
0702*3*Alfonso López 2° Etapa*7
0703*3*Alfonso López 3° Etapa*7
0704*1*Puerto Nuevo*7
0705*2*Puerto Mallarino*7
0706*2*Urbanización El Angel del Hogar*7
0707*3*Siete de Agosto*7
0708*2*Los Pinos*7
0709*2*San Marino*7
0710*3*Las Ceibas*7
0797*3*Parque de la Caña*7
0798*3*Fepicol*7
0801*2*Primitivo Crespo*8
0802*3*Simón Bolívar*8
0803*2*Saavedra Galindo*8
0804*2*Uribe Uribe*8
0805*3*Santa Mónica Popular*8
0806*3*La Floresta*8
0807*3*Benjamín Herrera*8
0808*3*Municipal*8
0809*2*Industrial*8
0810*3*El Troncal*8
0811*3*Las Américas*8
0812*3*Atanasio Girardot*8
0813*3*Santa Fe*8
0814*3*Chapinero*8
0815*3*Villa Colombia*8
0816*3*EL Trébol*8
0817*3*La Base*8
0818*2*Urbanización La Nueva Base*8
0901*3*Alameda*9
0902*3*Bretaña*9
0903*3*Junín*9
0904*3*Guayaquil*9
0905*3*Aranjuez*9
0906*3*Manuel María Buenaventura*9
0907*3*Santa Mónica Belalcazar*9
0908*3*Belalcazar*9
0909*1*Sucre*9
0910*2*Barrio Obrero*9
1001*3*El Dorado*10
1002*3*El Guabal*10
1003*3*La Libertad*10
1004*3*Santa Elena*10
1005*3*Las Acacias*10
1006*3*Santo Domingo*10
1007*3*Jorge Zawadsky*10
1008*4*Olímpico*10
1009*3*Cristobal Colón*10
1010*3*La Selva*10
1011*4*Barrio Departamental*10
1012*4*Pasoancho*10
1013*3*Panamericano*10
1014*4*Colseguros Andes*10
1015*3*San Cristobal*10
1016*3*Las Granjas*10
1017*3*San Judas Tadeo I*10
1018*3*San Judas Tadeo II*10
1101*3*Barrio San Carlos*11
1102*3*Maracaibo*11
1103*3*La Independencia*11
1104*3*La Esperanza*11
1105*3*Urbanización Boyacá*11
1106*3*El Jardín*11
1107*3*La Fortaleza*11
1108*3*El Recuerdo*11
1109*3*Aguablanca*11
1110*3*El Prado*11
1111*3*20 de Julio*11
1112*3*Prados de Oriente*11
1113*3*Los Sauces*11
1114*3*Villa del Sur*11
1115*3*José Holguín Garcés*11
1116*2*León XIII*11
1117*3*José María Cordoba*11
1118*2*San Pedro Claver*11
1119*2*Los Conquistadores*11
1120*2*La Gran Colombia*11
1121*2*San Benito*11
1122*2*Primavera*11
1201*2*Villanueva*12
1202*2*Asturias*12
1203*2*Eduardo Santos*12
1204*2*Barrio Alfonso Barberena A.*12
1205*3*El Paraiso*12
1206*3*Fenalco Kennedy*12
1207*3*Nueva Floresta*12
1208*2*Julio Rincón*12
1209*3*Doce de Octubre*12
1210*2*El Rodeo*12
1211*3*Sindical*12
1212*3*Bello Horizonte*12
1301*2*Ulpiano Lloreda*13
1302*1*El Vergel*13
1303*2*El Poblado I*13
1304*2*El Poblado II*13
1305*2*Los Comuneros II Etapa*13
1306*2*Ricardo Balcazar*13
1307*2*Omar Torrijos*13
1308*2*El Diamante*13
1309*2*Lleras Restrepo*13
1310*3*Villa del Lago*13
1311*2*Los Robles*13
1312*2*Rodrigo Lara Bonilla*13
1313*1*Charco Azul*13
1314*2*Villablanca*13
1315*3*Calipso*13
1390*2*Yira Castro*13
1393*1*Lleras Restrepo II Etapa*13
1394*2*Marroquín III*13
1395*2*Los Lagos*13
1397*1*Sector Laguna del Pondaje*13
1398*2*El Pondaje*13
1399*2*Sect. Asprosocial-Diamante*13
1401*1*Alfonso Bonilla Aragón*14
1402*2*Alirio Mora Beltran*14
1403*1*Manuela Beltran*14
1404*1*Las Orquídeas*14
1405* 2*José Manuel Marroquín II Etapa*14
1406*2*José Manuel Marroquín I Etapa*14
1495*1*Puerta del Sol*14
1496*1*Los Naranjos I*14
1498*1*Promociones Populares B*14
1499*1*Los Naranjos II*14
1501*1*El Retiro*15
1502*2*Los Comuneros I Etapa*15
1503*1*Laureano Gómez*15
1504*2*El Vallado*15
1596*3*Ciudad Cordoba*15
1598*1*Mojica*15
1599*2*El Morichal*15
1601*2*Mariano Ramos*16
1602*2*República de Israel*16
1603*2*Unión de Vivienda Popular*16
1604*2*Antonio Nariño*16
1605*2*Brisas del Limonar*16
1697*4*Ciudad 2000*16
1698*3*La Alborada*16
1701*3*La Playa*17
1702*4*Primero de Mayo*17
1703*3*Ciudadela Comfandi*17
1774*4*Caney*17
1775*4*Lili*17
1778*5*Santa Anita - La Selva*17
1780*5*El Ingenio*17
1781*5*Mayapan - Las Vegas*17
1782*5*Las Quintas de Don Simón*17
1783*5*Ciudad Capri*17
1784*5*La Hacienda*17
1785*5*Los Portales - Nuevo Rey*17
1786*3*Cañaverales - Los Samanes*17
1787*3*El Limonar*17
1788*4*Bosques del Limonar*17
1789*5*El Gran Limonar - Cataya*17
1790*5*El Gran Limonar*17
1791*5*Unicentro Cali*17
1793*5*Ciudadela Pasoancho*17
1794*5*Prados del Limonar*17
1796*5*Urbanización San Joaquin*17
1801*3*Buenos Aires*18
1802*3*Barrio Caldas*18
1803*3*Los Chorros*18
1804*3*Meléndez*18
1805*3*Los Farallones*18
1807*3*Francisco Eladio Ramirez*18
1808*2*Prados del Sur*18
1809*3*Horizontes*18
1810*2*Mario Correa Rengifo*18
1811*2*Lourdes*18
1812*3*Colinas del Sur*18
1813*3*Alferez Real*18
1814*3*Napoles*18
1815*2*El Jordán*18
1890*1*Sector Alto de Los Chorros*18
1896*3*Sector Meléndez*18
1897*1*Sector Alto Jordán*18
1898*1*Alto Nápoles *18
1901*4*El Refugio*19
1902*5*La Cascada*19
1903*5*El Lido*19
1904*5*Urbanización Tequendama*19
1905*4*Barrio Eucarístico*19
1906*4*San Fernando Nuevo*19
1907*4*Urbanización Nueva Granada*19
1908*4*Santa Isabel*19
1909*2*Bellavista*19
1910*5*San Fernando Viejo*19
1911*4*Miraflores*19
1912*4*3 de Julio*19
1913*4*El Cedro*19
1914*4*Champagnat*19
1915*4*Urbanización Colseguros*19
1916*4*Los Cambulos*19
1917*2*El Mortiñal*19
1918*4*Urbanización Militar*19
1919*5*Cuarto de Legua - Guadalupe*19
1921*5*Nueva Tequendama *19
1922*5*Camino Real - Joaquín Borrero S.*19
1923*5*Camino Real - Los Fundadores*19
1981*6*Altos de Santa Isabel -  La Morelia*19
1982*6*Santa Barbara*19
1983*6*Tejares - Cristales*19
1984*4*Unidad Residencial Santiago de Cali*19
1985*4*Unidad Residencial El Coliseo*19
1988*5*Cañaveralejo - Seguros Patria*19
1992*6*Cañaveral*19
1994*5*Pampa Linda*19
1995*5*Sector Cañaveralejo Guadalupe Antigua*19
1997*1*Sector Bosque Municipal*19
2001*2*El Cortijo*20
2002*3*Belisario Caicedo*20
2003*1*Siloé*20
2004*1*Lleras Camargo*20
2005*1*Belén*20
2006*1*Brisas de Mayo*20
2007*1*Tierra Blanca*20
2008*1*Pueblo Joven*20
2098*1*Venezuela - Urb Cañaveralejo*20
2099*1*La Sultana*20
2101*1*Pízamos I*21
2102*1*Pízamos II*21
2103*1*Calimio Desepaz*21
2104*1*El Remanso*21
2105*1*Los Lideres*21
2106*1*Desepaz Invicali*21
2107*1*Compartir*21
2108*1*Ciudad Talanga*21
2194*1*Villamercedes I-Villa Luz- Las Garzas*21
2195*1*Pízamos III - Las Dalias*21
2196*1*Potrero Grande*21
2197*2*Ciudadela del Río*21
2198*2*Valle Grande*21
2201*6*Urbanización Ciudad Jardín*22
2296*6*Parcelaciones Pance*22
2297*6*Urbanización Río Lili*22
2298*6*Ciudad Campestre*22
2299*6*Club Campestre*22
