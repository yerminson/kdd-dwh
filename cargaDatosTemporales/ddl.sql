--ddl para las tablas de datos temporales
DROP TABLE programa_temp CASCADE;
DROP TABLE asignatura_temp CASCADE;
DROP TABLE docente_temp CASCADE;
DROP TABLE estudiante_temp CASCADE;
DROP TABLE matricula_temp CASCADE;
DROP TABLE email_temp CASCADE;
DROP TABLE colegio_temp CASCADE;
DROP TABLE barrio_temp CASCADE;

--tabla programa_temp
CREATE TABLE programa_temp
(
	codigo_programa CHAR(4),
	nombre_facultad VARCHAR(60),
	nombre_programa VARCHAR(110)
);
ALTER TABLE programa_temp ADD CONSTRAINT programa_temp_pk PRIMARY KEY (codigo_programa);


--tabla asignatura_temp
CREATE TABLE asignatura_temp
(
	cod_asignatura VARCHAR(7),
	nombre_asignatura VARCHAR(90),
	creditos INTEGER,
	nombre_facultad VARCHAR(60),
	codigo_escuela_ofrece VARCHAR(5),
	nombre_escuela_ofrece VARCHAR(60)
);
ALTER TABLE asignatura_temp ADD CONSTRAINT asignatura_temp_pk PRIMARY KEY (cod_asignatura);


--tabla docente
CREATE TABLE docente_temp
(
	id INTEGER,
	nombre_docente VARCHAR(60)
);
ALTER TABLE docente_temp ADD CONSTRAINT docente_temp_pk PRIMARY KEY (id);


--tabla estudiante
CREATE TABLE estudiante_temp
(
   codigo CHAR(7),
   cod_programa CHAR(4),
   jornada_programa CHAR(3),
   nombre VARCHAR(15),
   primer_apellido VARCHAR(15),
   segundo_apellido VARCHAR(15),
   genero char(1),
   direccion VARCHAR(50),
   documento_identidad VARCHAR(10)
);
ALTER TABLE estudiante_temp ADD CONSTRAINT estudiante_temp_pk PRIMARY KEY (codigo);
ALTER TABLE estudiante_temp ADD CONSTRAINT cod_programa_estudiante_temp_fk FOREIGN KEY (cod_programa) REFERENCES programa_temp (codigo_programa);


--tabla matricula
CREATE TABLE matricula_temp
(
	cod_estudiante CHAR(7),
	cod_prog_academico CHAR(4),
	jornada_programa CHAR(3),
	cod_asignatura VARCHAR(7),
	grupo INTEGER,
	tipo_matricula VARCHAR(2),
	fecha_matricula TIMESTAMP,
	fecha_cancelacion TIMESTAMP,
	fecha_reactivacion TIMESTAMP
);
ALTER TABLE matricula_temp ADD CONSTRAINT matricula_temp_pk PRIMARY KEY (cod_estudiante, fecha_matricula);
ALTER TABLE matricula_temp ADD CONSTRAINT cod_estudiante_matricula_temp_fk FOREIGN KEY (cod_estudiante) REFERENCES estudiante_temp (codigo);
ALTER TABLE matricula_temp ADD CONSTRAINT cod_prog_academico_matricula_temp_fk FOREIGN KEY (cod_prog_academico) REFERENCES programa_temp (codigo_programa);
ALTER TABLE matricula_temp ADD CONSTRAINT cod_asignatura_matricula_temp_fk FOREIGN KEY (cod_asignatura) REFERENCES asignatura_temp (cod_asignatura);

--tabla email
CREATE TABLE email_temp
(
   email VARCHAR(50)
);
ALTER TABLE email_temp ADD CONSTRAINT email_temp_pk PRIMARY KEY (email);

--tabla colegio
CREATE TABLE colegio_temp
(
   colegio VARCHAR(80)
);
ALTER TABLE colegio_temp ADD CONSTRAINT colegio_temp_fk PRIMARY KEY (colegio);

--tabla barrios_cali
CREATE TABLE barrio_temp
(
   codigo CHAR(4),
   estrato INTEGER,
   nombre VARCHAR(40),
   comuna VARCHAR(2)
);
ALTER TABLE barrio_temp ADD CONSTRAINT barrio_temp_pk PRIMARY KEY (codigo);
