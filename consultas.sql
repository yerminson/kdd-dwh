--Consulta 

--Uso franja en cancelaciones y matriculas
select totales.total, datos.nombre_franja from
  (select sum(total_matriculas + total_cancelaciones) as total, franja_horaria_key
  from agregado_matricula_adicion_cancelacion 
  group by franja_horaria_key 
  having sum(total_matriculas + total_cancelaciones) >0) as totales
natural join
  (select franja_horaria_key , nombre_franja
  from franja_horaria) as datos 
  order by totales.total asc 
  limit 10;

--Uso franja solo en matriculas
select totales.total, datos.nombre_franja from
  (select sum(total_matriculas) as total, franja_horaria_key
  from agregado_matricula_adicion_cancelacion 
  group by franja_horaria_key 
  having sum(total_matriculas) >0) as totales
natural join
  (select franja_horaria_key , nombre_franja
  from franja_horaria) as datos 
   order by totales.total asc 
   limit 10;

--Uso franja solo en cancelaciones
select totales.total, datos.nombre_franja from
  (select sum(total_cancelaciones) as total, franja_horaria_key
  from agregado_matricula_adicion_cancelacion 
  group by franja_horaria_key 
  having sum(total_cancelaciones) >0) as totales
natural join
  (select franja_horaria_key , nombre_franja
  from franja_horaria) as datos 
   order by totales.total asc 
   limit 10;

--Uso asignaturas en matriculas y cancelaciones
select totales.total, datos.nombre from
(select sum(total_matriculas + total_cancelaciones) as total, asignatura_key
from agregado_matricula_adicion_cancelacion 
group by asignatura_key
having sum(total_matriculas + total_cancelaciones) >0) as totales
natural join
(select asignatura_key, nombre from asignatura ) as datos 
 order by totales.total asc 
 limit 10;

--Uso asignturas en matriculas 
select totales.total, datos.nombre from
(select sum(total_matriculas) as total, asignatura_key
from agregado_matricula_adicion_cancelacion 
group by asignatura_key
having sum(total_matriculas ) >0) as totales
natural join
(select asignatura_key, nombre from asignatura ) as datos 
 order by totales.total asc 
 limit 10;

--Uso asignaturas en cancelaciones
select totales.total, datos.nombre from
(select sum(total_cancelaciones) as total, asignatura_key
from agregado_matricula_adicion_cancelacion 
group by asignatura_key
having sum(total_cancelaciones) >0) as totales
natural join
(select asignatura_key, nombre from asignatura ) as datos 
 order by totales.total asc 
 limit 10;


--Uso programa en matriculas y cancelaciones
select totales.total, datos.nombre from
(select sum(total_matriculas + total_cancelaciones) as total, programa_key
from agregado_matricula_adicion_cancelacion 
group by programa_key
having sum(total_matriculas + total_cancelaciones) >0) as totales
natural join
(select programa_key, nombre from programa ) as datos order by totales.total limit 10;

--Uso programa en matriculas

select totales.total, datos.nombre from
(select sum(total_matriculas) as total, programa_key
from agregado_matricula_adicion_cancelacion 
group by programa_key
having sum(total_matriculas) >0) as totales
natural join
(select programa_key, nombre from programa ) as datos order by totales.total limit 10;


--Uso programa en cancelaciones
select totales.total, datos.nombre from
(select sum(total_cancelaciones) as total, programa_key
from agregado_matricula_adicion_cancelacion 
group by programa_key
having sum(total_cancelaciones) >0) as totales
natural join
(select programa_key, nombre from programa ) as datos order by totales.total limit 10;


--Uso dias en matriculas y cancelaciones
select sum(totales.total), datos.nombre_dia from
(select sum(total_matriculas + total_cancelaciones) as total, fecha_key
from agregado_matricula_adicion_cancelacion 
group by fecha_key
having sum(total_matriculas + total_cancelaciones) >0) as totales
natural join
(select fecha_key, nombre_dia  from fecha ) as datos group by datos.nombre_dia  order by sum(totales.total) limit 10;

--Uso dias en matriculas
select sum(totales.total), datos.nombre_dia from
(select sum(total_matriculas) as total, fecha_key
from agregado_matricula_adicion_cancelacion 
group by fecha_key
having sum(total_matriculas) >0) as totales
natural join
(select fecha_key, nombre_dia  from fecha ) as datos group by datos.nombre_dia  order by sum(totales.total) limit 10;

--Uso dias en cancelaciones
select sum(totales.total), datos.nombre_dia from
(select sum(total_cancelaciones) as total, fecha_key
from agregado_matricula_adicion_cancelacion 
group by fecha_key
having sum(total_cancelaciones) >0) as totales
natural join
(select fecha_key, nombre_dia  from fecha ) as datos group by datos.nombre_dia  order by sum(totales.total) limit 10;

--Uso dia que mas se cancelo y matriculo

select totales.total, datos.numero_dia, datos.numero_mes, datos.numero_anio from
(select sum(total_matriculas + total_cancelaciones) as total, fecha_key
from agregado_matricula_adicion_cancelacion 
group by fecha_key having sum(total_cancelaciones) > 0) as totales
natural join
(select fecha_key, numero_dia, numero_mes, numero_anio  from fecha ) as datos order by totales.total limit 10;

--Uso dia uqe mas se matriculo
select totales.total, datos.numero_dia, datos.numero_mes, datos.numero_anio from
(select sum(total_matriculas) as total, fecha_key
from agregado_matricula_adicion_cancelacion 
group by fecha_key having sum(total_cancelaciones) > 0) as totales
natural join
(select fecha_key, numero_dia, numero_mes, numero_anio  from fecha ) as datos order by totales.total limit 10;


--Uso dia que mas se cancelo
select totales.total, datos.numero_dia, datos.numero_mes, datos.numero_anio from
(select sum(total_cancelaciones ) as total, fecha_key
from agregado_matricula_adicion_cancelacion 
group by fecha_key having sum(total_cancelaciones) > 0) as totales
natural join
(select fecha_key, numero_dia, numero_mes, numero_anio  from fecha ) as datos order by totales.total limit 10;

--Comparar dos asignaturas con respecto a los programas.
select nombre, suma, suma2 from (select sum(total_cancelaciones) as suma, programa_key from agregado_matricula_adicion_cancelacion natural join (select asignatura_key from asignatura where nombre='CALCULO I') as j  group by programa_key) as a natural join (select sum(total_cancelaciones) as suma2, programa_key from agregado_matricula_adicion_cancelacion natural join (select asignatura_key from asignatura where nombre='ALGORITMIA Y PROGRAMACION')as k group by programa_key) as d natural join (select programa_key, nombre from programa) as t;

--Una sola asignatura
select * from (select sum(total_cancelaciones) as suma, programa_key from agregado_matricula_adicion_cancelacion natural join (select asignatura_key from asignatura where nombre='CALCULO I') as j  group by programa_key) as a natural join (select programa_key, nombre from programa) as t;

--Comparar dos asignaturas con respecto a las escuelas.
select sum(a.suma)as suma, sum(d.suma2)as suma2, nombreE from (select sum(total_cancelaciones) as suma, programa_key from agregado_matricula_adicion_cancelacion natural join (select asignatura_key from asignatura where nombre='CALCULO I') as j  group by programa_key) as a natural join (select sum(total_cancelaciones) as suma2, programa_key from agregado_matricula_adicion_cancelacion natural join (select asignatura_key from asignatura where nombre='ALGORITMIA Y PROGRAMACION')as k group by programa_key) as d natural join (select programa_key, nombreE from programa natural join (select nombre as nombreE, escuela_key from escuela) as b ) as t group by nombreE;

--Una sola asignatura
select sum(a.suma), nombreE from (select sum(total_cancelaciones) as suma, programa_key from agregado_matricula_adicion_cancelacion natural join (select asignatura_key from asignatura where nombre='CALCULO I') as j  group by programa_key) as a natural join (select programa_key, nombreE from programa natural join (select nombre as nombreE, escuela_key from escuela) as b ) as t group by nombreE;

--Uso equipos universidad y exterior por programa en cancela o matricula
select nombre, u.equiposUnivalle , e.equiposExterior from (select programa_key, count(*) as equiposUnivalle from cancelacion natural join (select equipo_key from equipo where edificio!='10') as eq group by programa_key) as u natural join (select programa_key, count(*) as equiposExterior from cancelacion natural join (select equipo_key from equipo where edificio ='10') as eqq group by programa_key) as e natural join (select nombre, programa_key from programa) as p order by nombre;

--Uso equipos uni y ext por escuela en cancela o matricula
select nombre , sum(u.equiposUnivalle) as equiposUnivalle , sum(e.equiposExterior) as equiposExterior from (select programa_key, count(*) as equiposUnivalle from cancelacion natural join (select equipo_key from equipo where edificio!='10') as eq group by programa_key) as u natural join (select programa_key, count(*) as equiposExterior from cancelacion natural join (select equipo_key from equipo where edificio ='10') as eqq group by programa_key) as e natural join (select nombreE as nombre, programa_key from programa natural join (select nombre as nombreE, escuela_key from escuela) as escu ) as p group by nombre order by nombre;

--Uso equipos por franja horaria en cancela o matricula
select nombre , sum(u.equiposUnivalle) as equiposUnivalle , sum(e.equiposExterior) as equiposExterior from (select franja_horaria_key, count(*) as equiposUnivalle from cancelacion natural join (select equipo_key from equipo where edificio!='10') as eq group by franja_horaria_key) as u  natural join (select franja_horaria_key, count(*) as equiposExterior from cancelacion natural join (select equipo_key from equipo where edificio ='10') as eqq group by franja_horaria_key) as e natural join (select nombre_franja as nombre, franja_horaria_key from franja_horaria ) as f group by nombre order by nombre;

--Uso equipos por dia de la semana en cancela o matricula
select nombre , sum(u.equiposUnivalle) as equiposUnivalle , sum(e.equiposExterior) as equiposExterior from (select fecha_key, count(*) as equiposUnivalle from cancelacion natural join (select equipo_key from equipo where edificio!='10') as eq group by fecha_key) as u  natural join (select fecha_key, count(*) as equiposExterior from cancelacion natural join (select equipo_key from equipo where edificio ='10') as eqq group by fecha_key) as e natural join (select nombre_dia as nombre , fecha_key from fecha ) as f group by nombre order by nombre;


--Uso equipo por un programa en especifico por franja
select nombre, sum(u.equiposUnivalle) as equiposUnivalle , sum(e.equiposExterior) as equiposExterior from (select franja_horaria_key, count(*) as equiposUnivalle from cancelacion natural join (select programa_key from programa where nombre='QUIMICA') as p natural join (select equipo_key from equipo where edificio!='10') as eq group by franja_horaria_key) as u natural join (select franja_horaria_key, count(*) as equiposExterior from cancelacion natural join (select programa_key from programa where nombre='QUIMICA') as pp natural join (select equipo_key from equipo where edificio ='10') as eqq group by franja_horaria_key) as e natural join (select nombre_franja as nombre, franja_horaria_key from franja_horaria) as f group by nombre order by nombre;


--Uso equipo por un programa en especifico por dia de la semana
select nombre, sum(u.equiposUnivalle) as equiposUnivalle , sum(e.equiposExterior) as equiposExterior from (select fecha_key, count(*) as equiposUnivalle from cancelacion natural join (select programa_key from programa where nombre='QUIMICA') as p natural join (select equipo_key from equipo where edificio!='10') as eq group by fecha_key) as u natural join (select fecha_key, count(*) as equiposExterior from cancelacion natural join (select programa_key from programa where nombre='QUIMICA') as pp natural join (select equipo_key from equipo where edificio ='10') as eqq group by fecha_key) as e natural join (select nombre_dia as nombre, fecha_key from fecha) as f group by nombre order by nombre;



--Programas que no usan equipos en cancelaciones
select p.programa_key, p.nombre from programa as p where programa_key not in (select programa_key from cancelacion natural join (select equipo_key from equipo where edificio!='10') as e ) and programa_key in (select distinct(programa_key) from cancelacion);

--Programas que no usan equipos en cancelaciones y en matriculas

select p.programa_key, p.nombre from programa as p where programa_key not in (select programa_key from cancelacion natural join (select equipo_key from equipo where edificio!='10') as e natural join (select programa_key from matricula) as m ) and programa_key in (select distinct(programa_key) from cancelacion natural join matricula);



